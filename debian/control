Source: xlog
Section: hamradio
Priority: optional
Maintainer: Debian Hamradio Maintainers <debian-hams@lists.debian.org>
Uploaders:
 Chrysostomos Nanakos <cnanakos@debian.org>,
 Kamal Mostafa <kamal@whence.com>,
 tony mancill <tmancill@debian.org>,
Homepage: http://xlog.nongnu.org/
Standards-Version: 4.5.0
Build-Depends:
 debhelper-compat (= 13),
 dpkg-dev (>= 1.16.1~),
 libgtk2.0-dev (>=2.12.0),
 libhamlib-dev,
Vcs-Browser: https://salsa.debian.org/debian-hamradio-team/xlog
Vcs-Git: https://salsa.debian.org/debian-hamradio-team/xlog.git
Rules-Requires-Root: no

Package: xlog
Architecture: any
Pre-Depends:
 dpkg (>= 1.15.6~),
Depends:
 libjs-jquery,
 xlog-data,
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 cwdaemon,
 extra-xdg-menus,
 glabels,
Recommends:
 shared-mime-info,
 xdg-utils,
Description: GTK+ Logging program for Hamradio Operators
 xlog is a logging program for amateur radio operators which can be used
 for daily logging and contest. Logs are stored into a text file.
 .
 QSO's are presented in a list. Items in the list can be added, deleted or
 updated. For each contact, dxcc information is displayed and bearings and
 distance is calculated, both short and long path.
 .
 When hamlib is enabled through the menu, you can retrieve frequency, mode and
 signal-strength from your rig over the serial port.

Package: xlog-data
Architecture: all
Multi-Arch: foreign
Depends:
 hamradio-files,
 ${misc:Depends},
Description: data for xlog, a GTK+ Logging program for Hamradio Operators
 xlog is a logging program for amateur radio operators which can be used
 for daily logging and contest. Logs are stored into a text file.
 .
 QSO's are presented in a list. Items in the list can be added, deleted or
 updated. For each contact, dxcc information is displayed and bearings and
 distance is calculated, both short and long path.
 .
 When hamlib is enabled through the menu, you can retrieve frequency, mode and
 signal-strength from your rig over the serial port.
 .
 This package contains the manual, locale files, pixmaps and more for xlog,
 a logging program for amateur radio operators.
